goflags=-v -ldflags '-w -s'
bin/tgbot_plain: *.go src/*/*.go
	mkdir -p bin
	cd bin && go build ${goflags} -o . ../src/...
run: bin/tgbot_plain
	@$^ -token "${TOKEN}"
clean:
	${RM} -r bin
