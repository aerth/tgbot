package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	"gitlab.com/aerth/tgbot"
)

func main() {
	log.SetFlags(log.Lshortfile)
	var (
		token          = os.Getenv("TOKEN")
		fullMode       = false
		adminID  int64 = 0
		verbose        = os.Getenv("DEBUGTG") != ""
	)
	flag.BoolVar(&fullMode, "full", fullMode, "unlock all features")
	flag.StringVar(&token, "token", token, "talk to @BotFather")
	flag.Int64Var(&adminID, "admin", adminID, "admin user, get ID from /start")
	flag.BoolVar(&verbose, "v", verbose, "verbose logs")
	flag.Parse()

	errfn := func(b *tgbot.Bot, u tgbot.Update, err error) {
		if err == nil {
			return
		}
		if adminID != 0 {
			fromstr := strings.TrimSpace(fmt.Sprintf("%s %s", u.Message.From.FirstName, u.Message.From.LastName))
			_, er := b.SendText(adminID, "[err] from=%s chatid=%d userid=%d update=%s err=%s", u.Message.From.String(), u.Message.Chat.ID, u.Message.From.ID, fromstr, err)
			if er != nil {
				log.Printf("couldnt errfn: %v", er)
			}
		}
	}
	// connect to telegram
	bot, err := tgbot.NewBot(token, tgbot.Options{Verbose: verbose, ErrFn: errfn})
	if err != nil {
		log.Fatalln(err)
	}
	// This is a Handler function (a 'BotFn'), it can be a method on your custom type.
	// It responds to /start commands with a constant string.

	bot.Handle("start", func(b *tgbot.Bot, u tgbot.Update) error {
		msg := b.GetMsg(u) // or use u.Message, but it could be nil.
		_, err := b.SendText(msg.Chat.ID, "id=%d", msg.From.ID)
		return err
	})
	if fullMode {
		full(bot)
	} else {
		log.Println("try -full flag for more demo features")
	}

	// Preloads get called before your BotFn for each update.
	// Here is one that just logs everything
	bot.Preload(tgbot.LoggerStderr)

	if adminID != 0 {
		bot.SendText(adminID, "hello admin")
	}

	// Start() blocks until fatal error or bot.Close() is called
	log.Fatalln(bot.Start())

}

// full() opens the database and adds more handlers (includign a default handler)
func full(bot *tgbot.Bot) {
	// without -full flag, this bot doesn't do much.
	// DB requires initialization, do this once.
	if err := bot.OpenDB("tgbot.db"); err != nil {
		log.Fatalln(err)
	}

	// and bucket creation before using the bucket!!
	// otherwise your bot will panic when you read to a non-existance bucket
	// note: Update (writes) automatically create the bucket
	bot.DB().CreateBucket("words")

	// register the BotFns (defined below!)
	bot.Handle("whats", whatisFn)
	bot.Handle("whatis", whatisFn)
	bot.Handle("define", defineFn)
	bot.Handle("save", defineFn)
	bot.Handle("write", defineFn)
	bot.Handle("list", listFn)
	bot.HandleDefault(whatisFn)

}

// Here are example BotFns that use a file-based database for persistence
// to use, try: /save #rules no spam
// then, try sending only: #rules

// defineFn updates the database
func defineFn(b *tgbot.Bot, u tgbot.Update) error {
	var msg = b.GetMsg(u)
	fromID := msg.From.ID
	admins := b.GetChatAdmins(msg.Chat.ID)
	authed := false
	for _, v := range admins {
		if v.User.ID == fromID {
			authed = true
			break
		}
	}

	if !authed {
		log.Println("ignoring pleb")
		b.ReplyText(msg.Chat.ID, msg.MessageID, "/define hello welcome to the chat")
		return nil
	}
	var args = b.GetMsgArgs(msg)
	if len(args) < 2 {
		b.SendText(msg.Chat.ID, "/define hello welcome to the chat")
		return nil
	}
	word := args[0]
	definitionString := strings.TrimSpace(strings.TrimPrefix(msg.CommandArguments(), word))
	err := b.DB().Marshal("words", word, definitionString)
	if err != nil {
		return err
	}
	_, err = b.SendText(msg.Chat.ID, "defined: %q", word)
	return err
}

// whatisFn fetches from the database
func whatisFn(b *tgbot.Bot, u tgbot.Update) error {
	var (
		msg  = b.GetMsg(u)
		args = b.GetMsgArgs(msg)
	)

	if msg.Command() != "whatis" && len(args) == 0 {
		args = []string{msg.Command()}
	}
	// expecting '/whatis word'
	if len(args) != 1 {
		return nil
	}
	var (
		definition string
		word       = args[0]
		buf        = b.DB().Get("words", word)
	)
	if len(buf) == 0 {
		b.ReplyText(msg.Chat.ID, msg.MessageID, "nothing important")
		return nil
	}
	// json Unmarshal and b.DB().Get()
	err := b.DB().Unmarshal("words", word, &definition)
	if err != nil {
		return err
	}
	_, err = b.ReplyText(msg.Chat.ID, msg.MessageID, "**%s**: *%s*", word, definition)
	return err
}

// listFn lists all the things in the bucket
func listFn(b *tgbot.Bot, u tgbot.Update) error {
	var (
		msg = b.GetMsg(u)
		//args = b.GetMsgArgs(msg)
	)
	all := b.DB().Keys("words") // scan bucket for key/value pair keys
	if len(all) == 0 {
		_, err := b.ReplyText(msg.Chat.ID, msg.MessageID, "no definitions. use /define <word> definition...")
		return err
	}
	_, err := b.ReplyText(msg.Chat.ID, msg.MessageID, "**%d definitions for words**: *%s*", len(all), strings.Join(all, "\n"))
	return err

}
