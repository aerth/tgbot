// tgbot package is minimal but nice
package tgbot

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gitlab.com/aerth/simpledb"
)

// Bot
type Bot struct {
	// Options probably shouldn't be exported
	Options Options

	// ParseMode for SendText functions
	ParseMode string // default markdown

	bot      *tgbotapi.BotAPI
	errFn    ErrFn // for example to notify owner
	fns      map[string]BotFn
	preloads []BotFn
	db       *simpledb.SimpleDB
	ctx      context.Context
}

// Update alias
type Update = tgbotapi.Update

// BotFn is sort of like a http.HandlerFunc but for telegram!
type BotFn func(b *Bot, u Update) error
type ErrFn func(b *Bot, u Update, err error) // err is logged to stderr and then errfn(u,err)
type Message = tgbotapi.Message
// Options including timeout and proxy
type Options struct {
	Timeout    time.Duration
	HTTPClient interface {
		Do(*http.Request) (*http.Response, error)
	}
	ErrFn   ErrFn
	Verbose bool
	Context context.Context
}

// var _ Options = Options{HTTPClient: http.DefaultClient}

// NewBot requires a token from @botfather
// Optionally, an Options{} struct can be passed
func NewBot(token string, options ...Options) (*Bot, error) {
	if len(options) > 1 {
		panic("developer: too many options, use one or none.")
	}
	token = strings.TrimSpace(token)
	if token == "" {
		return nil, fmt.Errorf("empty bot token, talk to @botfather")
	}
	var optionpack Options
	if len(options) > 0 {
		optionpack = options[0]
	}
	if optionpack.HTTPClient == nil {
		optionpack.HTTPClient = http.DefaultClient
	}
	if optionpack.Context == nil {
		optionpack.Context = context.Background() // no cancel
	}
	bot, err := tgbotapi.NewBotAPIWithClient(token, tgbotapi.APIEndpoint, optionpack.HTTPClient)
	if err != nil {
		return nil, err
	}
	return &Bot{
		bot:       bot,
		Options:   optionpack,
		fns:       make(map[string]BotFn),
		ParseMode: tgbotapi.ModeMarkdown,
		errFn:     optionpack.ErrFn,
		ctx:       optionpack.Context,
	}, nil
}
func (b *Bot) GetChatAdmins(chatid int64) []tgbotapi.ChatMember {
	admins, err := b.Bot().GetChatAdministrators(tgbotapi.ChatAdministratorsConfig{ChatConfig: tgbotapi.ChatConfig{ChatID: chatid}})
	if err != nil {
		return nil
	}
	return admins
}

// Start handling requests (blocks until a fatal error)
func (b *Bot) Start() error {
	return b.startloop()
}

// Close calls b.Bot().StopReceivingUpdates(), and closes internal bolt database if it was loaded.
func (b *Bot) Close() error {
	b.bot.StopReceivingUpdates()
	if b.db == nil || b.db.D == nil {
		return nil
	}
	return b.db.Close()
}

// DB returns the file-based database interface. See b.DB().DB() for the underlying bbolt database.
func (b *Bot) DB() *simpledb.SimpleDB {
	return b.db
}

// OpenDB initializes a file-based database. Be sure to b.DB().CreateBucket(bucketName) before usage.
func (b *Bot) OpenDB(path string) error {
	db, err := simpledb.Load(path)
	if err != nil {
		return err
	}
	b.db = db
	return nil

}

func (b *Bot) handleUpdateInner(update Update) {
	if e := b.handleUpdate(update); e != nil && e != ErrFinished {
		if b.errFn != nil {
			b.errFn(b, update, e)
		} else {
			log.Printf("error handling update: %v", e)
		}
	}
}
func (b *Bot) startloop() error {
	updatechan := b.bot.GetUpdatesChan(tgbotapi.UpdateConfig{})
	for {
		select {
		case update, ok := <-updatechan:
			if !ok {
				log.Println("telegram update-channel closed")
				return io.EOF
			}
			go b.handleUpdateInner(update)
		case <-b.ctx.Done():
			log.Println("telegram ctx is done")
			return io.EOF
		}
	}
}

func (b *Bot) handleUpdate(u tgbotapi.Update) error {
	for _, v := range b.preloads {
		if e := v(b, u); e != nil {
			if e == ErrFinished {
				return e
			}
			return fmt.Errorf("preload caught snag: %v", e)
		}
	}
	if u.Message == nil && u.ChannelPost == nil {
		return nil
	}

	var msg = u.Message
	if msg == nil {
		msg = u.ChannelPost
	}
	cmd := msg.Command()
	fn, ok := b.fns[cmd]
	if ok {
		return fn(b, u)
	}

	fn, ok = b.fns[""]
	if ok {
		return fn(b, u)
	}

	return nil
}

// ErrFinished can be returned by a preloaded BotFn to signal it has finished without errors and has been handled successfully
var ErrFinished = errors.New("finished without error") // blocks further execution

// Preload a BotFn handler before responding to an update
// If a preloaded BotFn returns an error, it will not continue
// See ErrFinished and consider returning that error to block further handling of the update
func (b *Bot) Preload(fn BotFn) {
	log.Printf("registering preload middleware #%d", len(b.preloads)+1)
	b.preloads = append(b.preloads, fn)
}

// HandleDefault is called when no matching handler is registered
// You can use HandleDefault to set a default handler that includes a complex switch
// Or use it to respond with "command not found" or similar.
func (b *Bot) HandleDefault(fn BotFn) {
	log.Printf("registering default command")
	b.fns[""] = fn
}

// Handle a slash command (do not include the slash)
func (b *Bot) Handle(cmd string, fn BotFn) {
	if cmd == "" {
		b.HandleDefault(fn)
		return
	}
	log.Printf("registering '/%s' command", cmd)
	b.fns[cmd] = fn
}

// Send a tgbotapi.Chattable
func (b *Bot) Send(msg tgbotapi.Chattable) (tgbotapi.Message, error) {
	if b.Options.Verbose {
		chatmsg, ok := msg.(tgbotapi.MessageConfig)
		if ok {
			log.Printf("[debug] sending to %d: %s ", chatmsg.ChatID, chatmsg.Text)
		} else {

			log.Printf("[debug] sending [%T]", msg)
		}
	}
	return b.bot.Send(msg)
}

// SendText sends a new message to chatid
func (b *Bot) SendText(chatid int64, text string, i ...interface{}) (tgbotapi.Message, error) {
	msg, err := b.buildMessage(chatid, text, i...)
	if err != nil {
		return tgbotapi.Message{}, err
	}
	return b.Send(msg)
}

// ReplyText sends a reply to a messageID
func (b *Bot) ReplyText(chatid int64, replyTo int, text string, i ...interface{}) (tgbotapi.Message, error) {
	var m tgbotapi.Message
	if replyTo == 0 {
		return m, fmt.Errorf("ReplyText: replyTo is 0, not sending")
	}
	msg, err := b.buildMessage(chatid, text, i...)
	if err != nil {
		return m, fmt.Errorf("ReplyText: %v", err)
	}
	msg.ReplyToMessageID = replyTo
	m, err = b.Send(msg)
	if err != nil {
		return m, fmt.Errorf("ReplyText: %v", err)
	}
	return m, nil
}

func (b *Bot) buildMessage(chatid int64, text string, i ...interface{}) (tgbotapi.MessageConfig, error) {
	var msg tgbotapi.MessageConfig
	if text == "" && len(i) == 0 {
		return msg, fmt.Errorf("buildMessage: cant build empty message")
	}
	if chatid == 0 {
		return msg, fmt.Errorf("buildMessage: replyTo is 0, not sending")
	}
	switch {
	case text == "":
		msg = tgbotapi.NewMessage(chatid, fmt.Sprintln(i...))
	case len(i) == 0:
		msg = tgbotapi.NewMessage(chatid, text)
	default:
		msg = tgbotapi.NewMessage(chatid, fmt.Sprintf(text, i...))
	}
	msg.ParseMode = b.ParseMode
	return msg, nil
}

// Bot returns the underlying tgbotapi.BotAPI
func (b *Bot) Bot() *tgbotapi.BotAPI {
	return b.bot
}

// Log an update to stderr (use b.Preload(tgbot.LoggerStderr) instead)
func (b *Bot) Log(u Update) error {
	return LoggerStderr(b, u)
}
func LoggerStderr(b *Bot, u Update) error {
	var (
		msg    = b.GetMsg(u)
		chatID = msg.Chat.ID
		txt    = msg.Text
	)
	if txt == "" {
		txt = msg.Caption
	}
	if msg.From == nil {
		_, e := fmt.Fprintf(os.Stderr, "%s [%d] %q\n", time.Now().Format(time.Kitchen), chatID, txt)
		return e
	}
	usrnam := "/@"
	usrnam += msg.From.UserName
	firstlastname := strings.TrimSpace(fmt.Sprintf("%s %s", msg.From.FirstName, msg.From.LastName))
	if firstlastname == "" {
		firstlastname = msg.From.String()
	}
	if msg.From.UserName == "" {
		usrnam = ""
	}
	_, e := fmt.Fprintf(os.Stderr, "%s [%d%s] %s: %s\n",
		time.Now().Format(time.Kitchen),
		chatID,
		usrnam,
		firstlastname,
		txt)
	return e
}

func (b *Bot) GetMsg(u Update) *tgbotapi.Message {
	if u.Message != nil {
		return u.Message
	}
	if u.ChannelPost != nil {
		return u.ChannelPost
	}
	if u.EditedMessage != nil {
		return u.EditedMessage
	}
	if u.EditedChannelPost != nil {
		return u.EditedChannelPost
	}
	// hey, a non-nil msg with a chat...
	return &tgbotapi.Message{
		Chat: &tgbotapi.Chat{},
	}
}
func (b *Bot) GetMsgArgs(m *tgbotapi.Message) []string {
	return strings.Fields(m.CommandArguments())
}
func (b *Bot) GetChatID(m *tgbotapi.Message) int64 {
	if m.Chat != nil {
		return m.Chat.ID
	}
	return 0
}
