API stable

Go modules recommended

### Example

```

package main

import (
	"flag"
	"log"
	"os"
	"strings"

	"gitlab.com/aerth/tgbot"
)

func main() {
	var (
		token    = os.Getenv("TOKEN")
		fullMode = false
	)
	flag.BoolVar(&fullMode, "full", fullMode, "unlock all features")
	flag.StringVar(&token, "token", token, "talk to @BotFather")
	flag.Parse()

	// connect to telegram
	bot, err := tgbot.NewBot(token)
	if err != nil {
		log.Fatalln(err)
	}

	// This is a Handler function (a 'BotFn'), it can be a method on your custom type.
	// It responds to /start commands with a constant string.
	const ConstResponseExample = "It Works"
	bot.Handle("start", func(b *tgbot.Bot, u tgbot.Update) error {
		msg := b.GetMsg(u) // or use u.Message, but it could be nil.
		_, err := b.SendText(msg.Chat.ID, ConstResponseExample)
		return err
	})

	// Preloads get called before your BotFn for each update.
	// Here is one that just logs everything
	bot.Preload(tgbot.LoggerStderr)

	// Start() blocks until fatal error or bot.Close() is called
	log.Fatalln(bot.Start())
}
```



## close with ctrl+c

```go
	ctx := context.TODO()
	ctx, stop := signal.NotifyContext(ctx, os.Interrupt, syscall.SIGHUP)
	defer stop()
	bot, err := tgbot.NewBot(token, tgbot.Options{
		Context: ctx,
	// ...
```