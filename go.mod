module gitlab.com/aerth/tgbot

go 1.20

require (
	github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.5.1
	gitlab.com/aerth/simpledb v0.0.5
)

require (
	go.etcd.io/bbolt v1.3.7 // indirect
	golang.org/x/sys v0.8.0 // indirect
)
